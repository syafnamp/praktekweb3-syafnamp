const exp=require("express")
const appServer=exp()
const port=3000;
const parser=  require("body-parser")
const jsonBody=parser.json()



// parse application/json
appServer.use(parser.json())


appServer.get ('/',function(req,res){
    res.send("Ini Root Aplikasi");
})

appServer.get('/mahasiswa',function(req,res){
    var mhs=["Bright","Win","Dew","MJ"];
        res.send(mhs);
})

appServer.post('/TambahMahasiswa',jsonBody,function(req,res){
        res.send("Menambahkan Mahasiswa");
})

appServer.put('/MengubahMahasiswa',jsonBody,function(req,res){
    res.send("Mengubah Mahasiswa");
})

appServer.delete('/MenghapusMahasiswa',function(req,res){
    res.send("Menghapus Mahasiswa");
})

appServer.listen(port,function(req,res){
    console.log("App Is Running on Port :"+port)
})